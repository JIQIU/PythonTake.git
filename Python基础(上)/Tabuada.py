# coding=gbk
# 作业1：使用while和for…in两个循环分别输出四种九九乘法表效果（共计8个）。

# for…in的四种九九乘法表效果 j正i正 j正i反 j反i正 j反i反

# j正i正
for j in range(1,10):
	for i in range(1,j+1):
		print("{}*{}={:<4}".format(i,j,j*i),end="")
	print("")
print("")

# j反i反
for j in range(9,0,-1):
	print("{:8}".format("")*(9-j),end="") # 计算出空白位置
	for i in range(j,0,-1):
		print("{}*{}={:<4}".format(i,j,j*i),end="")
	print("")
print("")

# j正i反
for j in range(1,10):
	print("{:8}".format("")*(9-j),end="") # 计算出空白位置
	for i in range(j,0,-1):
		print("{}*{}={:<4}".format(i,j,j*i),end="")
	print("")
print("")

# j反i正
for j in range(9,0,-1):
	for i in range(1,j+1):
		print("{}*{}={:<4}".format(i,j,j*i),end="")
	print("")
print("")

# while的四种九九乘法表效果  j正i正 j正i反 j反i正 j反i反

# j正i正
j=1
while(j<=9):
	i=1
	while(i<=j):
		print("{}*{}={:<4}".format(i,j,j*i),end="")
		i+=1
	print("")
	j+=1
print("")

# j反i反
j=9
while(j>0):
	print("{:8}".format("")*(9-j),end="") # 计算出空白位置
	i=j
	while(i>0):
		print("{}*{}={:<4}".format(i,j,j*i),end="")
		i-=1
	print("")
	j-=1
print("")

# j正i反
j=1
while(j<=9):
	print("{:8}".format("")*(9-j),end="") # 计算出空白位置
	i=j
	while(i>0):
		print("{}*{}={:<4}".format(i,j,j*i),end="")
		i-=1
	print("")
	j+=1
print("")

# j反i正
j=9
while(j>0):
	i=1
	while(i<=j):
		print("{}*{}={:<4}".format(i,j,j*i),end="")
		i+=1
	print("")
	j-=1
print("")

