# coding=gbk
# 作业3：实现一个自动取款机的存取款模拟效果。要求有登陆和退出、查询余额、取钱，存钱等操作。

# ATM后台数据库 id cipher name money 
ATMDataBase = [
	{'id':'001','cipher':'123456','name':'张三','money':1000},
	{'id':'002','cipher':'123456','name':'李四','money':0},
	{'id':'003','cipher':'123456','name':'王五','money':999999}]

ATMUser = "" #操作用户

def ATM_Init():
	"""
	ATM初始化，显示操作指令
	"""
	print("-"*40)
	print("{0:1}{1:>20}{0:>19}".format("|","ATM"))
	print("{0:1}{1:<16}{2:>16}{0:1}".format("|","1、登陆","2、退出"))
	print("{0:1}{1:<16}{2:>16}{0:1}".format("|","3、存钱","4、取钱"))
	print("{0:1}{1:<33}{0:1}".format("|","5、查询余额"))
	print("-"*40)

def ATM_Login():
	"""
	ATM登陆
	"""
	#global ATMDataBase
	global ATMUser
	account = input("请您的输入账号：")
	cipher = input("请您的输入密码：")
	for v in range(len(ATMDataBase)):
		if ATMDataBase[v]["id"] == account and ATMDataBase[v]['cipher'] == cipher: #判断账号密码是否正确
			ATMUser = account # 记录操作用户id
			print("登陆成功，请继续操作！")
			break
		else:
			if input("输入的账号密码有误，是否重新输入 yes/no?  ") == "yes":
				ATM_Login() # 重新输入
				break

def ATM_SaveMoney():
	"""
	ATM存钱
	"""
	try:
		sunMoney = int(input("请输入存款金额："))
	except:
		print("请不要放入假钞！")
		ATM_SaveMoney()
		return

	for v in range(len(ATMDataBase)):
		if ATMUser == ATMDataBase[v]['id']: #调取数据库用户信息进行改写
			ATMDataBase[v]['money']+=sunMoney
			print("您成功存款{}元".format(sunMoney))
			break

def ATM_DrawMoney():
	"""
	ATM取钱
	"""
	try:
		sunMoney = int(input("请输入存款金额："))
	except:
		ATM_DrawMoney()
		return

	for v in range(len(ATMDataBase)):
		if ATMUser == ATMDataBase[v]['id']: #调取数据库用户信息进行改写
			if ATMDataBase[v]['money'] >=sunMoney:
				ATMDataBase[v]['money']-=sunMoney
				print("您成功取款{}元".format(sunMoney))
				break
			else:
				print("您的账户余额不足！")
				break
				

def ATM_BalanceInquiry():
	"""
	ATM查询余额
	"""
	for v in range(len(ATMDataBase)):
		if ATMUser == ATMDataBase[v]['id']: #调取数据库用户信息输出
			print("您的账户余额还有{}元".format(ATMDataBase[v]['money']))
			break

def isLogin():
	"""
	是否已经登陆
	"""
	if ATMUser == "":
		print("请先进行登陆！")
	return not ATMUser == ""

def ATM_Handle():
	"""
	获取ATM操作，执行相关指令
	"""
	key = input("请输入对应编号进行操作：")
	if key == "1":
		ATM_Login()
	elif key == "2":
		return
	elif key == "3":
		if isLogin():
			ATM_SaveMoney()
	elif key == "4":
		if isLogin():
			ATM_DrawMoney()
	elif key == "5":
		if isLogin():
			ATM_BalanceInquiry()
	else:
		print("无效操作，请重新输入")

	ATM_Handle() #重复获取用户操作 直到用户执行退出操作

ATM_Init()
ATM_Handle()