# coding=gbk
# 作业2：使用文件和目录操作，定义一个统计指定目录大小的函数（注意目录中还有子目录）。

import os #载入os模块

def StatsSize(varDir):
	"""
	统计目录大小
	varDir：需要统计大小的目录path
	返回值：目录字节大小，int类型
	"""
	filesize = 0
	pathlist = os.listdir(varDir) # 获取目录下所有路径
	for v in pathlist:
		file1=os.path.join(varDir,v)
		if os.path.isfile(file1): #判断是否是文件
			filesize+=os.path.getsize(file1) # 所有文件大小叠加
		elif os.path.isdir(file1): #判断是否是目录
			filesize+=StatsSize(file1) #统计次级目录，获取次级目录文件大小
	return filesize

mPath = os.path.abspath("../") #指定统计地址
size = StatsSize(mPath) #获取目录大小

print("{}目录大小 {}字节 {:0.1f}KB".format(os.path.basename(mPath),size,size/1024)) #输出目录大小