-- MySQL dump 10.16  Distrib 10.1.36-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: blogdb
-- ------------------------------------------------------
-- Server version	10.1.36-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `abstract` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `uid` int(10) unsigned DEFAULT NULL,
  `pcount` int(10) unsigned DEFAULT '0',
  `flag` tinyint(3) unsigned DEFAULT '0',
  `cdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` VALUES (1,'MySQL中显示乱码显示原因+解决办法','乱码导致原因','发的',1,2,1,'2018-10-26 11:10:27'),(2,'Python数据库支持','PyMySQL安装','数据库连接操作',1,3,1,'2018-10-26 11:14:13'),(3,'Python数据库支持','PyMySQL安装','数据库连接操作',2,4,1,'2018-10-26 11:14:29'),(4,'MySQL数据库基础','MySQL简介','MySQL数据库安装',5,9,1,'2018-10-26 11:15:30'),(5,'Python基础之导入Python模块','Python的模块导入','使用pip命令安装',2,10,1,'2018-10-26 11:16:59'),(6,'Python基础之异常处理','捕获异常','多层异常处理',5,4,1,'2018-10-26 11:17:42'),(7,'Python基础之面向对象','类的定义','继承与重写',2,15,1,'2018-10-26 11:18:22'),(8,'Python简单实现ATM自动存取款机','ATM后台数据库',' id cipher name money',6,12,1,'2018-10-26 11:19:15'),(9,'Python基础之文件操作','open()打开文件','os模块',6,25,1,'2018-10-26 11:19:55'),(10,'Git命令：Git配置文件+连接远程仓库+项目管理','Git命令：Git配置文件','设置提交代码时的用户信息',2,4,1,'2018-10-26 11:20:48'),(11,'Git上传Unity工程GitHub','准备工作','在Github上新建一个空仓库',2,12,1,'2018-10-26 11:21:33');
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'zhangsan','zs@Gmail.com','2018-10-24 23:24:10'),(2,'lisi','ls@Gmail.com','2018-10-24 23:46:01'),(3,'wangwu','ww@Gmail.com','2018-10-24 23:47:09'),(4,'zhaoli','zl@Gmail.com','2018-10-24 23:47:28'),(5,'qianqi','qq@Gmail.com','2018-10-24 23:47:42'),(6,'zhouba','zb@Gmail.com','2018-10-24 23:48:02');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-26 11:29:15
